var testsRunningInNode = (typeof global !== "undefined" ? true : false),
    chakram = (testsRunningInNode ? global.chakram : window.chakram),
    expect = (testsRunningInNode ? global.expect : window.expect);
const endpoint = "https://api.fpt.ai/v1";
const URL = endpoint + "/recognize";
const URLIntent = endpoint + "/recognize/intent";
const URLEntity = endpoint + "/recognize/entity";
const token = "RUxppTjKvMnBn6Rps3c83W7nzjoTewKd";
const content = "đăng kí mạng"
const description = "đăng kí mạng"
const utterances = "test add samples"
let requestData = {
  "text": content
};
let options = {
  headers : {
    'Authorization' : 'Bearer ' + token
  }
};
let optionsWrongTocken = {
  headers : {
    'Authorization' : 'Bearer abcd'
  }
};
let optionsEmptyTocken = {
  headers : {
    'Authorization' : 'Bearer '
  }
};

describe("FPT.AI Additional - Recognition", function() {

    describe("Intent", function() {

        var postRequest;

        before(function() {
          postRequest = chakram.post(URLIntent, requestData, options);
          postWrongTocken = chakram.post(URLIntent, requestData, optionsWrongTocken);
          postEmptyTocken = chakram.post(URLIntent, requestData, optionsEmptyTocken);
          postNoAuthen = chakram.post(URLIntent, requestData,);
        });

        it("Recognize - Intent Normal", function () {
            return expect(postRequest).to.have.status(200);
        });

        it("Recognize - Intent Wrong Token", function() {
            return expect(postWrongTocken).to.have.status(401);
        });

        it("Recognize - Intent Empty Token", function() {
            return expect(postEmptyTocken).to.have.status(401);
        });

        it("Recognize - Intent No Authen", function() {
            return expect(postNoAuthen).to.have.status(401);
        });

        it("Should respond with data matching the intents schema", function () {
            var expectedSchema = {
                type: "object",
                properties: {
                    intents: {
                        type: "array",
                        properties: {
                            label: {type: "string"},
                            confidence: {type: "string"},
                        },
                        required: ["label", "confidence"]
                    }
                },
                required: ["intents"]
            };
            return expect(postRequest).to.have.schema(expectedSchema);
        });

    });

    describe("Entity", function() {
        var postRequest;
        before(function() {
          postRequest = chakram.post(URLEntity, requestData, options);
          postWrongTocken = chakram.post(URLEntity, requestData, optionsWrongTocken);
          postEmptyTocken = chakram.post(URLEntity, requestData, optionsEmptyTocken);
          postNoAuthen = chakram.post(URLEntity, requestData,);
        });
        it("Recognize - Entity Normal", function () {
            return expect(postRequest).to.have.status(200);
        });

        it("Recognize - Entity Wrong Token", function() {
            return expect(postWrongTocken).to.have.status(401);
        });

        it("Recognize - Entity Empty Token", function() {
            return expect(postEmptyTocken).to.have.status(401);
        });

        it("Recognize - Entity No Authen", function() {
            return expect(postNoAuthen).to.have.status(401);
        });

        it("Should respond with data matching the entities schema", function () {
            var expectedSchema = {
                type: "object",
                properties: {
                    entities: {
                        type: "array",
                        properties: {
                            label: {type: "string"},
                            confidence: {type: "string"},
                        },
                        required: ["label", "confidence"]
                    }
                },
                required: ["entities"]
            };
            return expect(postRequest).to.have.schema(expectedSchema);
        });
    });

    describe("Recognize", function() {
        var postRequest;
        before(function() {
          postRequest = chakram.post(URL, requestData, options);
          postWrongTocken = chakram.post(URL, requestData, optionsWrongTocken);
          postEmptyTocken = chakram.post(URL, requestData, optionsEmptyTocken);
          postNoAuthen = chakram.post(URL, requestData,);
        });
        it("Recognize Normal", function () {
            return expect(postRequest).to.have.status(200);
        });

        it("Recognize Wrong Token", function() {
            return expect(postWrongTocken).to.have.status(401);
        });

        it("Recognize Empty Token", function() {
            return expect(postEmptyTocken).to.have.status(401);
        });

        it("Recognize No Authen", function() {
            return expect(postNoAuthen).to.have.status(401);
        });

        it("Should respond with data matching the Recognize schema", function () {
            var expectedSchema = {
                type: "object",
                properties: {
                    intents: {
                        type: "array",
                        properties: {
                            label: {type: "string"},
                            confidence: {type: "string"},
                        },
                        required: ["label", "confidence"]
                    },
                    entities: {
                        type: "array",
                        properties: {
                            label: {type: "string"},
                            confidence: {type: "string"},
                        },
                        required: ["label", "confidence"]
                    }
                },
                required: ["entities"]
            };
            return expect(postRequest).to.have.schema(expectedSchema);
        });
    });

});
