var testsRunningInNode = (typeof global !== "undefined" ? true : false),
    chakram = (testsRunningInNode ? global.chakram : window.chakram),
    expect = (testsRunningInNode ? global.expect : window.expect);
var label = "ss"
const URLSampleAdd = endpoint + "/intents/" + label + "/utterances";
describe("FPT.AI - Benchmark - Add Sample", function() {
    let optionss = {
      headers : {
        'Authorization' : 'Bearer ' + token
      }
    };
    describe("Sample - Add", function() {

        var postRequest, responseData;

        before(function() {
          postRequest = chakram.post(URLSampleAdd, {
            "utterances": [utterances]
          }, optionss);
          postEmpty = chakram.post(URLSampleAdd, {
            "utterances": []
          }, optionss);
          postWrongTocken = chakram.post(URLSampleAdd, {
             "utterances": [utterances]
          }, optionsWrongTocken);
          postEmptyTocken = chakram.post(URLSampleAdd, {
            "utterances": [utterances]
          }, optionsEmptyTocken);
          postNoAuthen = chakram.post(URLSampleAdd,{
            "utterances": [utterances]
          },);
        });

        it("Sample - Create Normal", function () {
            expect(postRequest).to.have.status(200);
            return chakram.wait();
        });

        it("Sample - Intent Wrong Token", function() {
            return expect(postWrongTocken).to.have.status(401);
        });

        it("Sample - Intent Empty Token", function() {
            return expect(postEmptyTocken).to.have.status(401);
        });

        it("Sample - Intent No Authen", function() {
            return expect(postNoAuthen).to.have.status(401);
        });

        it("Sample - Create Empty", function() {
            return expect(postEmpty).to.have.status(200);
        });


    });

});
