var testsRunningInNode = (typeof global !== "undefined" ? true : false),
    chakram = (testsRunningInNode ? global.chakram : window.chakram),
    expect = (testsRunningInNode ? global.expect : window.expect);
//const endpoint = "https://api.fpt.ai/v1";
const URLIntents = endpoint + "/intents";
var labels = "ans_sssd";

describe("FPTAI - Benchmark - Add Intents", function() {
    let optionss = {
      headers : {
        'Authorization' : 'Bearer ' + token
      }
    };
    describe("Intent - Add", function() {

        var postRequest, responseData;

        before(function() {
          postRequest = chakram.post(URLIntents, {
            "label": labels,
            "description": "đăng kí qua mạng url"
          }, optionss);
          postEmpty = chakram.post(URLIntents, {
            "label": "",
            "description": ""
          }, optionss);
          postEmptyLabel = chakram.post(URLIntents, {
            "label": "",
            "description": description
          }, optionss);
          postEmptyDescription = chakram.post(URLIntents, {
            "label": labels,
            "description": ""
          }, optionss);

          postWrongTocken = chakram.post(URLIntents, {
            "label": labels,
            "description": "đăng kí qua mạng url"
          }, optionsWrongTocken);
          postEmptyTocken = chakram.post(URLIntents, {
            "label": labels,
            "description": "đăng kí qua mạng url"
          }, optionsEmptyTocken);
          postNoAuthen = chakram.post(URLIntents,{
            "label": labels,
            "description": "đăng kí qua mạng url"
          },);
        });

        it("Intent - Create Normal", function () {
            var expectedSchema = {
                type: "object",
                properties: {
                  label: {type: "string"},
                  description: {type: "string"},
                  intent_code: {type: "string"},
                  created_time: {type: "string"},
                  application_code: {type: "string"},
                },
                required: ["label","description","intent_code","created_time","application_code"]
            };
            expect(postRequest).to.have.status(201);
            expect(postRequest).to.have.schema(expectedSchema);
            return chakram.wait();
        });

        it("Intent - Intent Wrong Token", function() {
            return expect(postWrongTocken).to.have.status(401);
        });

        it("Intent - Intent Empty Token", function() {
            return expect(postEmptyTocken).to.have.status(401);
        });

        it("Intent - Intent No Authen", function() {
            return expect(postNoAuthen).to.have.status(401);
        });

        it("Intent - Create Empty", function() {
            return expect(postEmpty).to.have.status(400);
        });

        it("Intent - Create Empty Label", function() {
            return expect(postEmptyLabel).to.have.status(400);
        });

        it("Intent - Create Empty Description", function() {
            return expect(postEmptyDescription).to.have.status(400);
        });



    });

});
