var testsRunningInNode = (typeof global !== "undefined" ? true : false),
    chakram = (testsRunningInNode ? global.chakram : window.chakram),
    expect = (testsRunningInNode ? global.expect : window.expect);
var label = "ans_sssd"
const URLIntentDelete = endpoint + "/intents/" + label;
describe("FPT.AI - Benchmark - Delete Intents", function() {

    describe("Intent - Delete", function() {

        var postRequest, responseData;
        let optionss = {
          headers : {
            'Authorization' : 'Bearer ' + token
          }
        };
        before(function() {
          postRequest = chakram.delete(URLIntentDelete, {
          }, optionss);
          postWrongTocken = chakram.delete(URLIntentDelete, {
            "label": label,
            "description": "đăng kí qua mạng url"
          }, optionsWrongTocken);
          postEmptyTocken = chakram.delete(URLIntentDelete, {
            "label": label,
            "description": "đăng kí qua mạng url"
          }, optionsEmptyTocken);
          postNoAuthen = chakram.delete(URLIntentDelete,{
            "label": label,
            "description": "đăng kí qua mạng url"
          },);
        });

        it("Intent - Delete Normal", function () {
            expect(postRequest).to.have.status(200);
            return chakram.wait();
        });

        it("Intent - Intent Wrong Token", function() {
            return expect(postWrongTocken).to.have.status(401);
        });

        it("Intent - Intent Empty Token", function() {
            return expect(postEmptyTocken).to.have.status(401);
        });

        it("Intent - Intent No Authen", function() {
            return expect(postNoAuthen).to.have.status(401);
        });

    });

});
