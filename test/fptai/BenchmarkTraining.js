var testsRunningInNode = (typeof global !== "undefined" ? true : false),
    chakram = (testsRunningInNode ? global.chakram : window.chakram),
    expect = (testsRunningInNode ? global.expect : window.expect);
//const endpoint = "https://api.fpt.ai/v1";
const URLTraining = endpoint + "/train";

describe("FPT.AI - Benchmark - Training", function() {
    let optionss = {
      headers : {
        'Authorization' : 'Bearer ' + token
      }
    };
    describe("Benchmark - Training", function() {

        var postRequest;

        before(function() {
          postRequest = chakram.post(URLTraining, {}, optionss);
          postWrongTocken = chakram.post(URLTraining, {}, optionsWrongTocken);
          postEmptyTocken = chakram.post(URLTraining, {}, optionsEmptyTocken);
          postNoAuthen = chakram.post(URLTraining,{},);
        });

        it("Training - Normal", function () {
            return expect(postRequest).to.have.status(200);
        });

        it("Training - Wrong Token", function() {
            return expect(postWrongTocken).to.have.status(401);
        });

        it("Training - Empty Token", function() {
            return expect(postEmptyTocken).to.have.status(401);
        });

        it("Training - No Authen", function() {
            return expect(postNoAuthen).to.have.status(401);
        });
    });

});
